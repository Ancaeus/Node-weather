FROM alpine

FROM alpine:3.8


# Optional Configuration Parameter
ARG dbhost
ARG dbuser
ARG dbpass
ENV edbhost=$dbhost
ENV edbuser=$dbuser
ENV edbpass=$dbpass



RUN apk update && apk add mysql-client
RUN apk add --no-cache \
    dumb-init 

ENTRYPOINT [ "/usr/bin/dumb-init" ]
CMD [ "mysql" ]




